﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimplePlatformController : MonoBehaviour {
//  Variables checking whether or no the Player is facing right/jumping
	[HideInInspector] public bool facingRight = false;
	[HideInInspector] public bool jump = false;

//  Variables controling how the player moves
	public float moveForce = 365f;
	public float maxSpeed = 5f;
	public float jumpForce = 1000f;
	public Transform groundCheck;

//  Variables referncing Game Components
	private bool grounded = false;
	private Animator anim;
	private Rigidbody2D rb2d;
	private SpriteRenderer spriteRenderer;




	void Start () 
	{
//  	Get the Components needed
		anim = GetComponent<Animator> ();
		rb2d = GetComponent<Rigidbody2D> ();
	}

	void Awake ()
	{
		spriteRenderer = GetComponent <SpriteRenderer>();
	}
 
	void Update () {
//		Not gonna lie I'm not sure what exactly this is, but it does something
		grounded = Physics2D.Linecast (transform.position, groundCheck.position, 1 << LayerMask.NameToLayer ("Ground"));

//		If the player presses up, they will jump
		if (Input.GetButtonDown ("Jump") && grounded) 
		{
			jump = true;
		}
	}

	void FixedUpdate()
	{
//		Making a var for the Horizontal axis
		float h = Input.GetAxis ("Horizontal");

//		Code for the animator (That is useless (but I can't remove it without Unity yelling at me))
		anim.SetFloat("Speed", Mathf.Abs(h));

//		Allows player to move
		if (h * rb2d.velocity.x < maxSpeed) 
		{
			rb2d.AddForce (Vector2.right * h * moveForce);
		}

		if (Mathf.Abs (rb2d.velocity.x) > maxSpeed) 
		{
			rb2d.velocity = new Vector2 (Mathf.Sign (rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);
		}

//		Flips image when Player turns
		if (h > 0 && !facingRight) 
		{
			Flip ();
		}
		else if (h < 0 && facingRight)
		{
			Flip ();
		}

//		More animation stuff
		if (jump)
		{
			anim.SetTrigger("Jump");
			rb2d.AddForce(new Vector2(0f, jumpForce));
			jump = false;
		}
	}

//	Function that controlls Player sprite
	void Flip()
	{
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
}
